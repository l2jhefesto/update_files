# ---------------------------------------------------------------------------
# Sistemas ja estavam presente no codigo
# ---------------------------------------------------------------------------
Ativar/desativar racas
Auto Potions
Boss Announcements
Champion Monster
Faction System
Fake Players
FindPvp
Multilingual system
Offline Trader
Premium System
Pvp Reward System
PvP Color System
Sell Buffs
Start Custom Location
Secondary Authentication
PvpFlagZone (custom)


# -------------------------------------------------
# Sistemas adicionais
# -------------------------------------------------
Dressme
Custom Disarm
Block Augment in oly
Limitacao custom para o recebimento de xp
Teleport BookMark
Painel de usuario
Sistema de Talentos (em desenvolvimento)
Arquivamento (em desenvolvimento)


# -------------------------------------------------
# Eventos
# -------------------------------------------------
Wendding System
Team Vs Team
Capture the Flag
Hitman
Luck Pig
Raid Event
Town War
AngelCat
Cath A Tiger
Character Birthday
Coffer of Shadows
Freya Celebration
Gift of Vitality
Heavy Medal
L2Day
Lover's Jubilee
Love Your Gatekeeper
Rudolph's Blessing
Saving Santa
The Valentine


# -------------------------------------------------
# Comandos
# -------------------------------------------------
.online
.panel
.lang
.deposit/.withdraw
.combinetalismans
.dressme


# -------------------------------------------------
# Npcs custom
# -------------------------------------------------
Npc Buffer
Npc Buffer Premium
Npc Delevel
Npcs Fantay Isle
